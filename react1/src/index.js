import React from 'react';
import ReactDOM from 'react-dom';

const HelloWorld = ()   => {
 return ( <div>
   <Hello/> <World/></div> );
}

const Hello = () => {
  let isHello = false;
  return (
    <span>{isHello ? 'Hello' : 'Not Hello'} </span>
  );
}

const World = () => {
  const value = 'World';
  return (
    <span> {value} </span>
  );
}


ReactDOM.render(
  <HelloWorld />,
  document.querySelector('#root')
);
