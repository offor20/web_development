import React from 'react';
import ReactDom from 'react-dom';
import './index.css';

function Tweet() {

    return (
        <div className="tweet">
            <Avatar/>
            <div className="content">
                <NameWithHandle/> <Time/>
                <Message/>

                <div className="button">
                    <LikeButton/>
                    <CommentButton/>
                    <RetweetButton/>
                    <ShareButton/>
               </div>
                
            </div>
            
        </div>

    );
    
}
function Message(){
    return(
        <div className="message" >
            Here goes the tweet message
        </div>
    ) ;
}

function NameWithHandle(){
    return (
        <span className="name-with-handle">
             <span className="name"> Ernest Ugwoke</span>
             <span className= "name">@fforernest</span>
        </span>
    
        )
}

function Avatar() {
    return (
        <img src="https://www.gravatar.com/avatar/nothing"
        className="avatar"
        alt="avatar"/>
    )

}

const Time = () => {
  //  let t = new Date().toLocaleString();

return (
    <span className= "time"> 6h ago </span>
);
}

const LikeButton = () => (
    <i className = "fa fa-heart like-button"/>
);

const CommentButton = () => (
    <i className = "far fa-comment"/>
);

const RetweetButton = () => (
    <i className = "fa fa-retweet retweet-button"/>
);

const ShareButton = () => (
    <i className="fas fa-external-link-alt"/>
);



ReactDom.render(<Tweet/>, document.querySelector('#root'));


